// //Setup the dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');//this allows your sites to connect to this server

const userRoutes = require('./routes/userRoutes');

const productRoutes = require('./routes/productRoutes');


// ======= DATABASE CONNECTION =======
//connection string from MongoDB Atlas

mongoose.connect('mongodb+srv://admin:admin@b106.kqte1.mongodb.net/capstone-2?retryWrites=true&w=majority', {
	useNewUrlParser: true,  //to use the new parser
	useUnifiedTopology: true, //to use the new server discover and monitoring engine
	useFindAndModify: false
})

mongoose.connection.once('open', ()=> console.log('Now Connected to Database'))

//server set up
const app = express();

//it allws resources sharing from all origins
app.use(cors())//tells the server that cors is being used by your server

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);



app.listen(process.env.PORT || 3000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 3000}`)

});
