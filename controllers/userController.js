const User = require('../models/user');
const Product = require('../models/product')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')

module.exports.emailExists = (body) => {
	return User.find({ email: body.email }).then(result => {
		return result.length > 0 ? (`User exists in the database`) : (`User doesn't exists in the database`)
	})
}


module.exports.register = (params) =>{
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10),
		//10 = salt/string of characters added to password before hashing
		isAdmin: params.isAdmin
	})


	return user.save().then((user, err)=>{
		return (err) ? false : (`New user successfully created`)
	})
}


module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then( user => {
		if (user === null) {
			return false
		}
			
			const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

			if(isPasswordMatched) {
				return { accessToken: auth.createAccessToken(user.toObject())}
					
			}else {
				return false
			}

			
		
	})
}

//retrieve user details using access token
//conceal user password
module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {

		user.password = undefined

		return user
	})
}


//user purchases a product
module.exports.checkout = async (data) => {


		
	let userSaveStatus = await User.findById(data.userId).then(user=> {

		user.orders.push({productId : data.productId});

		return user.save().then((user, error)=> {
			if(error) {
				return false;
			}else {
				return user;
			}
		})
	}) 

	let productSaveStatus = await Product.findById(data.productId).then(product=> {

		product.purchasee.push({userId : data.userId});

		return product.save().then((product, error)=> {
			if(error) {
				return false;
			}else {
				
				return product;
			}
		})
	})


	if(userSaveStatus && productSaveStatus) {
		return true;
	}else {
		return false;
	}
}


module.exports.getOrders = (params) => {
	
	return User.find({ orders : {$exists: true, $ne: []}}, { _id: 1, firstName: 1, lastName: 1, orders:1 }).then((orders,error) => {
		if(error) {
			console.log(error)
			return false;
		}else {
			return orders;
		}
	})
}
module.exports.myOrders = (data) => {

	return User.findById((data.userId), {_id:1,firstName: 1, lastName: 1, myOrders: 1}).then(userOrders => {
			return userOrders
			return userOrders.ProductId
	})
}
