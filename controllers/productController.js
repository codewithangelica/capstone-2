const Product = require('../models/product');
const User = require('../models/user');
const auth = require('../auth')

module.exports.productExists = (body) => {
	return Product.find({ name: body.name }).then(result => {
		return result.length > 0 ? (`Product exists in the database`) : (`Product doesn't exists in the database`)
	})
}

module.exports.addProduct = (params) =>{
	let newProduct = new Product({
		name: params.name,
		description: params.description,
		price: params.price,	
		createdOn: Date.now()
	})

	return newProduct.save().then((newProduct, err)=>{
			return (err) ? false : true
		
	})
}


module.exports.productAvailable = (params) => {
	return Product.find({isActive:true}).then((product,error) => {
		if(error) {
			console.log(error)
			return false;
		}else {
			return product;
		}
	})
}




module.exports.productUnavailable = (params) => {
	return Product.find({isActive:false}).then((product,error) => {
		if(error) {
			console.log(error)
			return false;
		}else {
			return product;
		}
	})
}


module.exports.getProduct = (productId) => {
	return Product.findById(productId).then((result, error) => {
		if(error) {
			return console.log(error)
			return false;
		}else 
			console.log(result)
			return result;
	})
}



module.exports.updateProduct = (productId, newContent) => {

	return Product.findById(productId).then((result, error)=>{
		if(error){
			console.log(error);
			return false;
		}
		
		result.name = newContent.name
		result.description = newContent.description
		result.price = newContent.price
		return result.save().then((updatedProduct, error)=>{
			if(error){
				console.log(error);
				return false;
			}else{
				return true;
			}
		})

	})
}


module.exports.archiveProduct = (productId) => {
	return Product.findByIdAndUpdate(productId, {isActive: false}).then((result, error) => {
		if(error) {
			return true
			console.log(error);

		}else {
			return false;
		}
	})
}
