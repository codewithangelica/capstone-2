
const express = require('express');
const router = express.Router();
const UserController = require('../controllers/userController');

const auth = require('../auth');



router.post('/email-exists', (req, res)=>{
	UserController.emailExists(req.body).then(result => res.send(result))
})


router.post('/', (req,res)=> {
	UserController.register(req.body).then(result=> res.send(result))
})



router.post('/login', (req, res)=>{
	UserController.login(req.body).then(result => res.send(result))
});



router.get('/details', auth.verify, (req, res)=> {
	const user = auth.decode(req.headers.authorization)
	UserController.get({ userId: user.id }).then(user => res.send(user))
});


router.post('/checkout', auth.verify, (req, res, next)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		//userId comes from the decoded JWT's id field
		productId: req.body.productId
		//productId comes from the request body
		
		
	}
	
	const user = auth.decode(req.headers.authorization)

	if(user.isAdmin === true) {
		return res.status(401).send('You should be a non-admin account to checkout.')
	}else {

		UserController.checkout(data).then(result => res.send(result))
	}
})

router.get('/orders', auth.verify, (req, res, next)=>{
	const user = auth.decode(req.headers.authorization)

	if(user.isAdmin === false) {
		return res.status(401).send('You are not authorized to perform the action.')
	}else {
		UserController.getOrders(req.body).then(result => res.send(result))
	}
})


router.get('/myOrders', auth.verify, (req, res, next)=>{
	const user = auth.decode(req.headers.authorization)

	if(user.isAdmin === true) {
		return res.status(401).send('Only non-admin can view own orders.')
	}else {
		UserController.myOrders({ userId: user.id }).then(result => res.send(result))
	}
})
module.exports = router



