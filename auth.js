const jwt = require('jsonwebtoken');
const secret = 'malupitnasecret';

//3 main parts
//1. creation of the token -> analogy: pack the gift, and sign with the secret

module.exports.createAccessToken = (user) =>{
	//the data from the user parameter from the login

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//create the token and using the jwt's sign function
	return jwt.sign(data, secret, {})
}




//2.Auth.js - Verification of the token -> analogy: receive the gift and verify if the sender is legitimate and the gift was not tampered with

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	if(typeof token !== 'undefined') {

		token = token.slice(7, token.length)
		//we slice because Bearer 8248924728947y98fhusgfewr89
		//Bearer (plus space) - we only want to get the token string and not the 'bearer ' anymore
		//Recall slice function for JS - cuts the string starting from the 1st value up to the specified value
		//This is because the first 7 characters is not relevant to the actual token
		//We don't need the word "Bearer " so we removed it using the slice
		
		return jwt.verify(token, secret, (err, data)=> {
			return (err) ? res.send({ auth: 'failed'}) :next()
			//next() is used to passes the request to the next callback function in the route
			//next() tells the server to allow us to proceed with the next request
		})
	}else {
		return res.send({auth: 'failed'})

	}
}



//3. Decoding of the Token -> open the gift and get the content
	module.exports.decode = (token) => {
		//check if the token is present or not
		if(typeof token !== 'undefined') {
			token = token.slice(7, token.length)

			return jwt.verify(token, secret, (err, data)=> {
				return (err) ? null : jwt.decode(token, { complete: true}).payload
				//{complete: true} grabs both the request header and the payload
			})
		}else {
			return null
		}
	}
	//jwt.decode -> decodes the token and get the payload
	//payload is the data from the token we create from createAccessToken
	//the one with _id, the email, and the isAdmin

