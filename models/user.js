const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, 'First name is required']
	},

	lastName: {
		type: String,
		required: [true, 'Last name is required']
	},

	email: {
		type: String,
		required: [true, 'Email is required']
	},

	password: {
		type: String,
		required: [true, 'Password is required']
	},

	mobileNo: {
		type: String,
		required: [true, 'Mobile number is required']
	},

	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			

			productId: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Product'
				
			},
			
			quantity: {
				type: Number,
				default: 1
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: 'Added to Cart' 
			}
		}

	]



})

module.exports = mongoose.model('User', userSchema)
