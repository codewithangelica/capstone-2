const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/productController');
const UserController = require('../controllers/userController');
const auth = require('../auth');


//Check if product exists
router.post('/product-exists', auth.verify, (req, res)=>{
	ProductController.productExists(req.body).then(result => res.send(result))
	
})

//Add a product and verify the User Token
router.post('/addProduct', auth.verify, (req, res, next)=>{
	const user = auth.decode(req.headers.authorization)
	if(user.isAdmin === false) {
		return res.status(401).send('unautorized transaction')
	}else {
		ProductController.addProduct(req.body).then(result => res.send(result))
	}
})



//getting details of all available products
router.get('/availableProducts', (req, res)=> {
	ProductController.productAvailable(req.body).then(result=> res.send(result))
	
})
//getting details of all unavailable/archived products
router.get('/unavailableProducts', (req, res)=> {
	ProductController.productUnavailable(req.body).then(result=> res.send(result))
	
})
//getting all details of single product using a Product Id
router.get("/:id", (req, res)=> {
	ProductController.getProduct(req.params.id).then(result=> res.send(result))
});



router.put('/:id', auth.verify, (req, res, next)=>{
	const user = auth.decode(req.headers.authorization)
	if(user.isAdmin === false) {
		return res.status(401).send('You are not authorized to perform the action.')
	}else {
		ProductController.updateProduct(req.params.id, req.body).then(result => res.send(result))
	}
})


//Archive/Soft Delete a product and Admin is only authorized to do the action

router.delete('/:id/archive', auth.verify, (req, res, next)=>{
	const user = auth.decode(req.headers.authorization)

	if(user.isAdmin === false) {
		return res.status(401).send('You are not authorized to perform the action.')
	}else {
		ProductController.archiveProduct(req.params.id).then(result => res.send(result))
	}
})





//admin 
router.put("/:id/changeToAdmin", auth.verify, (req, res, next)=> {
	const user = auth.decode(req.headers.authorization)
	if(user.isAdmin === false) {
		return res.status(401).send('You are not authorized to perform the action.')
	}else {
		ProductController.changeToAdmin(req.params.id, req.body).then(result => res.send(result))
	}
})


module.exports = router;